package com.kshrd.btbrestfulapidemo.advice;


import com.kshrd.btbrestfulapidemo.dto.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice

public class GlobalExceptionHandler {

// methods for handling the exception....
    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<?> nullExceptionHandler(){
        ApiError error  =  new ApiError();

            error.setMessage("Null error!");
            error.setStatus(HttpStatus.BAD_REQUEST);

        return  ResponseEntity.ok().body(error);
    }

    @ExceptionHandler({ArithmeticException.class})
    public ResponseEntity<?> arithmeticExceptionHandler(){
        ApiError error  =  new ApiError();

        error.setMessage("Your Math is sucks!");
        error.setStatus(HttpStatus.NOT_FOUND);

        return  ResponseEntity.ok().body(error);
    }



}
