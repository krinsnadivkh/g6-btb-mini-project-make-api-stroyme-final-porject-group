package com.kshrd.btbrestfulapidemo.controller.restcontroller;


import com.kshrd.btbrestfulapidemo.dto.request.UserLoginRequest;
import com.kshrd.btbrestfulapidemo.dto.request.UserSingUpRequest;
import com.kshrd.btbrestfulapidemo.dto.respone.UserLoginResponse;
import com.kshrd.btbrestfulapidemo.dto.respone.UserSingUpRespone;
import com.kshrd.btbrestfulapidemo.model.Role;
import com.kshrd.btbrestfulapidemo.model.User;
//import com.kshrd.btbrestfulapidemo.model.request.UserLoginRequest;
//import com.kshrd.btbrestfulapidemo.model.response.UserLoginResponse;
//import com.kshrd.btbrestfulapidemo.security.UserDetailImp;
import com.kshrd.btbrestfulapidemo.security.jwt.JwtUtils;
import com.kshrd.btbrestfulapidemo.service.RolseService;
import com.kshrd.btbrestfulapidemo.service.UserService;
import com.kshrd.btbrestfulapidemo.utils.reponse.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class AuthRestController {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;

    @Autowired
    private RolseService rolseService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    ModelMapper modelMapper = new ModelMapper();


    @PostMapping("/login")
    public Response<UserLoginResponse> login (@RequestBody UserLoginRequest request){
     try{

        User findUserByUsername = userService.findUserByuserName(request.getUsername ());

        if(findUserByUsername == null || !passwordEncoder.matches ( request.getPassword ( ),findUserByUsername.getPassword ()) ){
            return Response.<UserLoginResponse>exception().setError("Incorrect user or password please check !!");
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
        );

        JwtUtils jwtUtils  = new JwtUtils();
        String token = jwtUtils.generateJwtToken(authentication);
        UserLoginResponse loginResponse;

        try{
            loginResponse = modelMapper.map(findUserByUsername,UserLoginResponse.class);
        }catch (Exception exception){
            System.out.println("Error when finding the user by username");
            return Response.<UserLoginResponse>exception().setError("Failed to find the user by the provided username !");
        }

        loginResponse.setToken(token);

        return Response.<UserLoginResponse>ok().setPayload(loginResponse).setError("Login successfully ...");
    }catch (Exception exception){

        System.out.println("Exception occur when trying to login for this credential : "+ exception.getMessage());
        return Response.<UserLoginResponse>exception().setError("Failed to perform the the authentication due to the exception issues !");

        }



    }



    @PostMapping("/singup")
    public Response<Object> singUp (@RequestBody UserSingUpRequest userSingUpRequest)
    {

            if(userSingUpRequest.getRoles().isEmpty())
            {
                return Response.exception().setError("Please specity role be for sing up");
            }
        try {
            for(var tmp:userSingUpRequest.getRoles())
            {
               Role role=  userService.findUserRoleByName(tmp);
               if(role==null)
               {
                   return Response.exception().setError("Sorry Role name "+tmp+ " doesn't exits!!!");
               }
            }

//                userSingUpRequest.getPassword()
            userSingUpRequest.setPassword(passwordEncoder.encode((userSingUpRequest.getPassword())));
//            user.setPassword ( passwordEncoder.encode ( user.getPassword () ) );

            int user_id = userService.singUpUser(userSingUpRequest);
            for(var tmp:userSingUpRequest.getRoles())
            {
                Role role=  userService.findUserRoleByName(tmp);
                System.out.println("Role id"+ role.getId());
                System.out.println("user id"+ user_id);
                userService.createUserRole(role.getId(),user_id);
            }


            UserSingUpRespone userSingUpRespone = modelMapper.map(userService.findUserByuserName(userSingUpRequest.getUsername()),UserSingUpRespone.class);
            System.out.println("0Heknfkd"+userSingUpRespone);
            return Response.exception().setPayload(userSingUpRespone).setError("Sing up Successfull !!!!");
        }
        catch (Exception ex)
        {
            return Response.exception().setError(ex.getMessage());
        }

    }




}















//    @Autowired
/*
    StudentRepository studentRepository;
*/

//    ModelMapper modelMapper = new ModelMapper();
//
//    @PostMapping("/login")
//    Response<UserLoginResponse>login (@RequestBody UserLoginRequest request){
//
//        UsernamePasswordAuthenticationToken  usernamePasswordAuthenticationToken = new
//                UsernamePasswordAuthenticationToken(
//                        request.getUsername(),request.getPassword()
//        );
//        try{
//
//            Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
//
//            JwtUtils jwtUtils = new JwtUtils();
//            String token = jwtUtils.generateJwtToken(authentication);
//
//            UserLoginResponse response = new UserLoginResponse();
//
//            System.out.println("Here is the value of the token : "+token);
//
//            try{
//                UserResponse findUserByUsername =studentRepository.findByUsername(request.getUsername());
//               // response.setUsername(findUserByUsername.getUsername());
//                response = modelMapper.map(findUserByUsername,UserLoginResponse.class);
//                response.setToken(token);
//                return Response.<UserLoginResponse>ok().setPayload(response).setError("Successfully login....");
//
//            }catch (Exception ex){
//                System.out.println("Fialed to find the user with the provided id: "+ex.getMessage());
//                return Response.<UserLoginResponse>exception().setError("Cannot find user by the given username");
//            }
//        }catch (Exception exception){
//
//            System.out.println("Error when user trying to log in : "+ exception.getMessage());
//            return Response.<UserLoginResponse>exception().setError("Failed to login , exception occur");
//
//        }
//
//    }
//
//
//    @GetMapping("/check")
//    String checkToken (@AuthenticationPrincipal UserDetailImp login){
//
//        try {
//
//            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//            System.out.println("Here is the value of the login User : ");
//            System.out.println(login);
//
//            if (login.getAuthorities().contains("USER")){
//                return "You are the User";
//            }else {
//                return "You are not the User ";
//            }
//
//
//        }catch (Exception ex){
//
//            System.out.println("Error when authenticate the user : "+ex.getMessage());
//            return "Error";
//        }
//
//    }
//}
