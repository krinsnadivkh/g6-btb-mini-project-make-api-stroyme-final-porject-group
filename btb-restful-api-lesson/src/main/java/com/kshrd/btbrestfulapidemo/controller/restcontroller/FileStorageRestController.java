package com.kshrd.btbrestfulapidemo.controller.restcontroller;

import com.kshrd.btbrestfulapidemo.dto.respone.UploadFileResponse;
import com.kshrd.btbrestfulapidemo.service.fileservice.FileStorageService;
import com.kshrd.btbrestfulapidemo.utils.reponse.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api/v1/file")
public class FileStorageRestController {
    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping("/file-upload")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);

        String fileUrl = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/images/")
                .path(fileName)
                .toUriString();
        return new UploadFileResponse(fileName, fileUrl,
                file.getContentType(), file.getSize());
    }
}
//



















