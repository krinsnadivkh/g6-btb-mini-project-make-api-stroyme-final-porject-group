package com.kshrd.btbrestfulapidemo.controller.restcontroller;

import com.kshrd.btbrestfulapidemo.dto.request.PostRequest;
import com.kshrd.btbrestfulapidemo.dto.respone.PostResponse;
import com.kshrd.btbrestfulapidemo.model.Comment;
import com.kshrd.btbrestfulapidemo.model.Post;
import com.kshrd.btbrestfulapidemo.repository.PostRepository;
import com.kshrd.btbrestfulapidemo.security.UserDetailImp;
import com.kshrd.btbrestfulapidemo.service.CommentService;
import com.kshrd.btbrestfulapidemo.service.PostService;
import com.kshrd.btbrestfulapidemo.service.UserService;
import com.kshrd.btbrestfulapidemo.utils.paging.Paging;
import com.kshrd.btbrestfulapidemo.utils.reponse.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/post")
public class PostRestController {

//    @Autowired
//    PostService postService;
@Autowired
private UserService userService;
//@Autowired
//private PostService postService;
@Autowired
private CommentService commentService;

@Autowired
private PostService postService;
    @PostMapping("/create-post")
    public Response<PostResponse> createPost(@RequestBody PostRequest postRequest, @AuthenticationPrincipal UserDetailImp singin) {
        if (postRequest == null) {
//            return Response.exception().setError("E");

            return Response.<PostResponse>exception().setError("Please specifies your post body");
        }

        try {
            Post userPost = new Post();
            userPost.setCaption(postRequest.getContent());
            userPost.setImage(postRequest.getImage());
            userPost.setUser_id(singin.getId());

//            int post_id = postService.cratePost(userPost);
            int post_id = userService.cratePost(userPost);
            System.out.println("past is id :"+post_id);
            PostResponse postResponse;
          postResponse =userService.findPostByID(post_id);
//            postResponse=postService.findPostByID(post_id);
            postResponse.setOwner(true);
            postResponse.setUsername(singin.getUsername());
            return Response.<PostResponse>ok().setPayload(postResponse).setError("Post successfully ...");

        } catch (Exception e) {
            return Response.<PostResponse>exception().setError(e.getMessage());
//        return null;
        }
    }



    @GetMapping("/find-all")
    public Response<List<PostResponse>> findAllPosts(
            @RequestParam(defaultValue ="10") int limit,
                                                     @RequestParam(defaultValue = "1")  int page,
                                                     @org.springframework.security.core.annotation.AuthenticationPrincipal UserDetailImp login){

        List<PostResponse> findAllPost;

        try {

            Paging paging =  new Paging();
            paging.setPage(page);
            int offset = (page -1)* limit;
            paging.setLimit(limit);

            findAllPost = postService.findAllPost (limit,offset);

            for(int i=0; i<findAllPost.size (); i++){
                if(findAllPost.get (i).getUer_id()== login.getId ())
                    findAllPost.get ( i ).setOwner ( true );
            }

            return Response.<List<PostResponse>>ok().setPayload(findAllPost).setError("User fetched successfully!").setMetadata(paging);

        }catch (Exception ex){
            System.out.println(" Fetching data exception: "+ex.getMessage());
            return Response.<List<PostResponse>>exception().setError("Failed to retrieve the post due the sql exception...");
        }
    }

//    @GetMapping("/{id}/view")
//    public Response<List<PostResponse>> viewBYid()
//    {
//        return Response.ok();
//    }

    @GetMapping("/{id}/view")
    public Response<PostResponse> findPostById(@PathVariable int id, @org.springframework.security.core.annotation.AuthenticationPrincipal UserDetailImp singin){
        try{
            PostResponse postResponse;
            postResponse = userService.findPostByID ( id );

            if(postResponse == null)
                return Response.<PostResponse>exception().setError("Can't find this post please check again ...");

            if(singin.getId () == postResponse.getUer_id());
                postResponse.setOwner ( true );

            return Response.<PostResponse>ok().setPayload(postResponse).setError("Students fetched successfully!");
        }
        catch ( Exception ex ){
            System.out.println(" Fetching data exception: "+ex.getMessage());
            return Response.<PostResponse>exception().setError("Failed to retrieve the post due the sql exception...");
        }
    }


    @DeleteMapping("{id}/delete-post")
    public Response<String> deletePost(@PathVariable int id, @org.springframework.security.core.annotation.AuthenticationPrincipal UserDetailImp login) {
        try {

            Post post = userService.getPostById(id);
            if (post == null)
                return Response.<String>exception().setError("Can't find this post please check again ...");

            if (post.getUser_id() != login.getId())
                return Response.<String>exception().setError("Permission denied for delete this post ...");

            userService.deletePostById(id);
            return Response.<String>ok().setError("Delete post successfully");

        } catch (Exception ex) {
            System.out.println(" Fetching data exception: " + ex.getMessage());
            return Response.<String>exception().setError("Failed to retrieve the post due the sql exception...");
        }

    }
    @PatchMapping("{id}/update-post")
    public Response<String> UpdatePost(@PathVariable int id, @org.springframework.security.core.annotation.AuthenticationPrincipal UserDetailImp singin) {
        try {

            Post post = userService.getPostById(id);
            if (post == null)
                return Response.<String>exception().setError("Can't find this post please check again ...");

            if (post.getUser_id() != singin.getId())
                return Response.<String>exception().setError("Permission denied for delete this post ...");

            userService.deletePostById(id);
            return Response.<String>ok().setError("Delete post successfully");

        } catch (Exception ex) {
            System.out.println(" Fetching data exception: " + ex.getMessage());
            return Response.<String>exception().setError("Failed to retrieve the post due the sql exception...");
        }
    }





//    comment section
@PostMapping("/{post_id}/comment")
public Response<Comment> commentOnPost(@PathVariable(name = "post_id") int id, @RequestParam String content, @org.springframework.security.core.annotation.AuthenticationPrincipal UserDetailImp singin){
    try{
        Comment comments = new Comment ();
        comments.setPost_id (id);
        comments.setContent (content);
        comments.setUser_id (singin.getId ( ));

        int commentId = commentService.creatComment (comments);

        comments = commentService.findCommentById (commentId);
        System.out.println("commment id"+commentId);
        System.out.println("post id"+comments.getPost_id());

        return Response.<Comment>ok().setPayload ( comments ).setError("Comment on post successfully ...");

    }catch ( Exception ex ){
        System.out.println(" Comment on post exception: "+ex.getMessage());
        return Response.<Comment>exception().setError(ex.getMessage());
    }

}


    @DeleteMapping("/delete-comment")
    public Response<String> deleteCommentOrReply(@RequestParam int c_id, @org.springframework.security.core.annotation.AuthenticationPrincipal UserDetailImp singin  ){

        try{

            Comment comments = commentService.findCommentById(c_id);

            if(comments != null) {

                if (comments.getUser_id () == singin.getId ()) {
                    commentService.deleteCommentById ( c_id );
                    return Response.<String>ok ( ).setError ( "Comment delete successfully ..." );
                }
                else
                    return Response.<String>exception ( ).setError ( "You can't delete other comment ..." );
            }
            else
                return Response.<String>exception ( ).setError ( "Comment you want to delete doesn't exit ..." );

        }catch(Exception ex){
            System.out.println (ex.getMessage () );
            return Response.<String>exception().setError ( "Fail to perform delete comment dou to exception ..." );
        }

    }










}
