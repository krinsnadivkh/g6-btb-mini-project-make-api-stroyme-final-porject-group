package com.kshrd.btbrestfulapidemo.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class PostRequest {
    private String content;
    private String image;
}
