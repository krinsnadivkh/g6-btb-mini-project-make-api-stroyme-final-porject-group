package com.kshrd.btbrestfulapidemo.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserSingUpRequest {
    private String full_name;
    private String username;
    private String password;
    private List<String> roles;
    private LocalDateTime create_date;
    private String image;

}
