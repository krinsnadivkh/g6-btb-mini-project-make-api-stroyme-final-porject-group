package com.kshrd.btbrestfulapidemo.dto.respone;

import com.kshrd.btbrestfulapidemo.model.Comment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PostResponse {
    private int id;
    private String caption;
    private String image;
    private boolean owner;
    private int uer_id;
    private String username;
    List<Comment> comment = new ArrayList<>();

}
