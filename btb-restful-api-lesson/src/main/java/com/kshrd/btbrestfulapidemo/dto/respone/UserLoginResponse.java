package com.kshrd.btbrestfulapidemo.dto.respone;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserLoginResponse {
    private int id;
    private String full_name;
    private String username;
    private String token;
    private List<String> roles;
}
