package com.kshrd.btbrestfulapidemo.dto.respone;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserSingUpRespone {
    private int id;
   private String username;
//    private String password;
    private List<String> roles;
    private String image;

}
