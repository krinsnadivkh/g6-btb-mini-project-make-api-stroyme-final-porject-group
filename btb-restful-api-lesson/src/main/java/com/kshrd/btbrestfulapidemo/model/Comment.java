package com.kshrd.btbrestfulapidemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Comment {
    private int id;
    private String content;
    private int user_id;
    private int post_id;
    private int parent_id;

}
