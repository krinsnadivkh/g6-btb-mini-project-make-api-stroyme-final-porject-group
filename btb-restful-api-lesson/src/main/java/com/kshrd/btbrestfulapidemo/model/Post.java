package com.kshrd.btbrestfulapidemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Post {
    private int id;
    private String caption;
    private String image;
    private int user_id;
    private String post_date;
    }
