package com.kshrd.btbrestfulapidemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Role
{
    private  int id ;
    private String name;
}
