package com.kshrd.btbrestfulapidemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {
    private int id;
    private String full_name;
    private String username;
    private String password;
    private String image;
    private boolean status;
    private LocalDateTime create_date;
    private List<String> roles;
}

