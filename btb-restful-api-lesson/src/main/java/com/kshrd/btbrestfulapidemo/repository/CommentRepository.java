package com.kshrd.btbrestfulapidemo.repository;

import com.kshrd.btbrestfulapidemo.model.Comment;
import org.apache.ibatis.annotations.*;

@Mapper
public interface CommentRepository {


    @Select( "insert into comments (content, post_id, user_id) " +
            "values (#{com.content}, #{com.post_id}, #{com.user_id}) returning id" )
    int creatComment(@Param("com") Comment comment);


//    @Select ( "select c.*, p.parent_comment_id from comments AS c left join parent_child_comment p ON " +
//            "c.id = p.child_comment_id where p.parent_comment_id = #{id}" )
    @Select ( "select * from comments where id=#{id}" )
    Comment findCommentById(int id);

    @Delete("Delete from comments where id=#{id}")
    int deleteCommentById(int id);

}
