package com.kshrd.btbrestfulapidemo.repository;

import com.kshrd.btbrestfulapidemo.dto.respone.PostResponse;
import com.kshrd.btbrestfulapidemo.model.Post;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
public interface PostRepository {

    @Select ( "insert into post(caption, status, user_id, image) " +
            "values (#{p.caption}, true, #{p.user_id}, #{p.image}) returning id" )
    public int cratePost(@Param("p") Post post);

//    @Select ( "insert into post(caption, status, user_id, image) " +
//            "values (#{post.caption}, true, #{post.user_id}, #{post.image}) returning id" )
//    int addPost(@Param("post") Post post);

@Select("SELECT * from  post where id=#{id}")
//@Results({
//        @Result(property = "username", column = "username"),
//        @Result(property = "totalLike", column = "id", one = @One(select = "findTotalLikePost")),
//        @Result(property = "comment", column = "id", many = @Many(select = "com.example.spring_security_jwt.repository.post.CommentRepository.findCommentByPostId"))
//})
    PostResponse findPostByID(int id);
@Select("SELECT * from  post where id=#{id}")
Post getPostById(int id);

@Delete("Delete from post where id=#{id}")
int deletePostById(int id);



 @Select ( "select count(*) from post_like where post_id = #{id}" )
int indTotalLikePost(int id);

 @Update("update post set caption='up',image='d' where id=#{id}")
    int updatePost(int id);

@Select("select post.*, tb_users.username from post inner join tb_users on tb_users.id = post.user_id limit #{limit} offset #{offset}")
List<PostResponse> findAllPost(int limit, int offset);
}
