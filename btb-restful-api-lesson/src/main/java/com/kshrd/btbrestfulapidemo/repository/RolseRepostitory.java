package com.kshrd.btbrestfulapidemo.repository;

import com.kshrd.btbrestfulapidemo.model.Role;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


import java.util.List;

@Mapper
public interface RolseRepostitory {

    @Select("select role_name from tb_role inner join tb_user_role tur on tb_role.id = tur.role_id where user_id=#{id}")
    List<String> FindRolesById(int id);
//    @Insert( "insert into user_role(user_id, role_id) values (#{userId}, #{roleId})" )
//    int addUserRole(int userId, int roleId);


    @Insert("Insert into tb_user_role(role_id,user_id) values(#{role_id},#{user_id})")
    int addUserRole(int role_id,int user_id);

    @Select("select * from tb_role where role_name = #{name}")
    Role findRoleByName(String name);

    @Insert("Insert into tb_user_role(role_id,user_id) values(#{role_id},#{user_id})")
    int createUserRole(int role_id,int user_id);


}
