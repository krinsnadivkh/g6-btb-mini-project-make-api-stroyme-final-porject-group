package com.kshrd.btbrestfulapidemo.repository;

import com.kshrd.btbrestfulapidemo.dto.request.UserSingUpRequest;
import com.kshrd.btbrestfulapidemo.model.Role;
import com.kshrd.btbrestfulapidemo.model.User;
import org.apache.ibatis.annotations.*;

@Mapper
public interface UserRepository {

    @Select("Select * from tb_users where username = #{username}")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "roles",column = "id",many = @Many(select = "com.kshrd.btbrestfulapidemo.repository.RolseRepostitory.FindRolesById"))
            }
    )
    User findUserByuserName(String username);


    @Select("insert into tb_users (full_name,username,password,image,status,create_date) values (#{user.full_name},#{user.username},#{user.password},#{user.image},'true',#{user.create_date}) returning id")
    int singUpUser(@Param("user")UserSingUpRequest userSingUpRequest);



}
