package com.kshrd.btbrestfulapidemo.repository.providers;

import org.apache.ibatis.jdbc.SQL;

public class StudentProvider {

    public String getAllStudentWithPagination(int limit, int offset, String filter){

        return new SQL(){{

            if (!filter.isEmpty()){

                SELECT("*");
                FROM("students");
                LIMIT("#{limit}");
                OFFSET("#{offset}");
                WHERE("UPPER(bio) like UPPER('%'||#{filter}||'%')");

            }else {
                SELECT("*");
                FROM("students");
                LIMIT("#{limit}");
                OFFSET("#{offset}");
            }

        }}.toString();
    }


}
