package com.kshrd.btbrestfulapidemo.security;

import com.kshrd.btbrestfulapidemo.model.AuthUser;
import com.kshrd.btbrestfulapidemo.model.User;
import com.kshrd.btbrestfulapidemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserDetailServiceImp implements UserDetailsService {

    @Autowired
//StudentRepository repository;
    UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User loginUser = repository.findUserByuserName(username);

        if (loginUser==null)
            throw new UsernameNotFoundException("Sorry, Cannot Find this User ...");

        List<GrantedAuthority> authorities = loginUser.getRoles()
                .stream()
                .map(e-> new SimpleGrantedAuthority(e))
                .collect(Collectors.toList());


//        System.out.println(" Here is the authority value : ");
//        authorities.stream().forEach(System.out::println);

        return new UserDetailImp(loginUser.getId(), loginUser.getUsername(),loginUser.getPassword(),loginUser.getFull_name(),authorities);

    }

}
