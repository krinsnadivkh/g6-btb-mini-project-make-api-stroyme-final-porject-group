package com.kshrd.btbrestfulapidemo.security.jwt;

import com.kshrd.btbrestfulapidemo.security.UserDetailImp;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Component
@Slf4j
public class JwtUtils{

    private String jwtSecret="ThisIsSecret";
    private long jwtExpiration=86400000; // 3600ms = 24000 hours

    public String generateJwtToken(Authentication authentication){

        UserDetailImp userPrinciple=(UserDetailImp) authentication.getPrincipal();
        return Jwts
                .builder()
                .setSubject(userPrinciple.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime()+jwtExpiration))
                .signWith(SignatureAlgorithm.HS512,jwtSecret)
                .compact();

    }

    // Get usename by the provided token
    public String getUsernameFromJwtToken(String token){

        if (validateJwtToken(token)){

            return Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
        }else return  null;
    }




    // create method for validation the jwtToken
    public boolean validateJwtToken(String authToken){


        try {

            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return  true;
        }catch (SignatureException e){ log.error("Invalid JWT signature : {}", e.getMessage());}
        catch (MalformedJwtException e ){ log.error("Invalid JWT token : {}",e.getMessage());}
        catch (ExpiredJwtException e){ log.error("JWT token is expired: {}",e.getMessage());}
        catch (UnsupportedJwtException e){ log.error("JWT token is unsupported : {}",e.getMessage());}
        catch (IllegalArgumentException e){log.error("JWT claims string is empty : {}",e.getMessage());}
        catch (Exception e){ log.error("Exception : {}"+ e.getMessage());}
        return false;
    }
}
