package com.kshrd.btbrestfulapidemo.service;

import com.kshrd.btbrestfulapidemo.model.Comment;
import org.springframework.stereotype.Service;

@Service
public interface CommentService {

    int creatComment(Comment comment);

    Comment findCommentById(int id);

    int deleteCommentById(int id);



}
