package com.kshrd.btbrestfulapidemo.service;

import com.kshrd.btbrestfulapidemo.dto.respone.PostResponse;
import com.kshrd.btbrestfulapidemo.model.Post;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface PostService {
    public int cratePost(Post  post);

//    PostResponse findPostByID(int id);
List<PostResponse> findAllPost(int limit, int offset);
}
