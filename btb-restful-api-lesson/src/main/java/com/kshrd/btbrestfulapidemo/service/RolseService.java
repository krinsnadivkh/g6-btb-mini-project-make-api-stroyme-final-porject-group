package com.kshrd.btbrestfulapidemo.service;

import com.kshrd.btbrestfulapidemo.dto.respone.PostResponse;
import com.kshrd.btbrestfulapidemo.model.Role;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RolseService{
    List<String> FindRolesById(int id);

    int addUserRole(int role_id,int user_id);

    Role findRoleByName (String name);
    int createUserRole (int role_id,int user_id);



}
