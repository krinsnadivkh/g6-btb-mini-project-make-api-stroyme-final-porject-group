package com.kshrd.btbrestfulapidemo.service;

import com.kshrd.btbrestfulapidemo.dto.request.UserSingUpRequest;
import com.kshrd.btbrestfulapidemo.dto.respone.PostResponse;
import com.kshrd.btbrestfulapidemo.model.Post;
import com.kshrd.btbrestfulapidemo.model.Role;
import com.kshrd.btbrestfulapidemo.model.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    User findUserByuserName(String username);

    int singUpUser(UserSingUpRequest userSingUpRequest);

    Role findUserRoleByName(String name);

    int createUserRole (int role_id,int user_id);

    public int cratePost(Post post);
    PostResponse findPostByID(int id);
   Post getPostById(int id);

    int deletePostById(int id);
    int updatePost(int id);


}
