package com.kshrd.btbrestfulapidemo.service.serviceImp;

import com.kshrd.btbrestfulapidemo.model.Comment;
import com.kshrd.btbrestfulapidemo.repository.CommentRepository;
import com.kshrd.btbrestfulapidemo.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommnetServiceImp implements CommentService {

@Autowired
    CommentRepository commentRepository;
    @Override
    public int creatComment(Comment comment) {
        return commentRepository.creatComment(comment);
    }

    @Override
    public Comment findCommentById(int id) {
        return commentRepository.findCommentById(id);
    }

    @Override
    public int deleteCommentById(int id) {
        return commentRepository.deleteCommentById(id);
    }
}
