package com.kshrd.btbrestfulapidemo.service.serviceImp;

import com.kshrd.btbrestfulapidemo.dto.respone.PostResponse;
import com.kshrd.btbrestfulapidemo.model.Post;
import com.kshrd.btbrestfulapidemo.repository.PostRepository;
import com.kshrd.btbrestfulapidemo.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PostServiceImp implements PostService {
    @Autowired
    PostRepository postRepository;
    @Override
    public int cratePost(Post post) {
        return postRepository.cratePost(post);
    }

    @Override
    public List<PostResponse> findAllPost(int limit, int offset) {
        return postRepository.findAllPost(limit,offset);
    }

//    @Override
//    public PostResponse findPostByID(int id) {
//        return postRepository.findPostByID(id);
//    }


}
