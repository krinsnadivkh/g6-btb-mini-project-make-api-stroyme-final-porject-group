package com.kshrd.btbrestfulapidemo.service.serviceImp;

import com.kshrd.btbrestfulapidemo.model.Role;
import com.kshrd.btbrestfulapidemo.repository.RolseRepostitory;
import com.kshrd.btbrestfulapidemo.service.RolseService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor

public class RolseServiceImp implements RolseService {
    @Autowired
    RolseRepostitory rolseRepostitory;

    @Override
    public List<String> FindRolesById(int id) {
        return rolseRepostitory.FindRolesById(id);
    }

    @Override
    public int addUserRole(int role_id, int user_id) {
        return rolseRepostitory.addUserRole(role_id,user_id);
    }


    @Override
    public Role findRoleByName(String name) {
        return rolseRepostitory.findRoleByName(name);
    }

    @Override
    public int createUserRole(int role_id, int user_id) {
        return rolseRepostitory.createUserRole(role_id,user_id);
    }


}
