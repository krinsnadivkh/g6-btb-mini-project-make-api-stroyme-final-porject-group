package com.kshrd.btbrestfulapidemo.service.serviceImp;

import com.kshrd.btbrestfulapidemo.dto.request.UserSingUpRequest;
import com.kshrd.btbrestfulapidemo.dto.respone.PostResponse;
import com.kshrd.btbrestfulapidemo.model.Post;
import com.kshrd.btbrestfulapidemo.model.Role;
import com.kshrd.btbrestfulapidemo.model.User;
import com.kshrd.btbrestfulapidemo.repository.PostRepository;
import com.kshrd.btbrestfulapidemo.repository.RolseRepostitory;
import com.kshrd.btbrestfulapidemo.repository.UserRepository;
import com.kshrd.btbrestfulapidemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserServiceImp implements UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RolseRepostitory rolseRepostitory;
    @Autowired
    PostRepository postRepository;
    @Override
    public User findUserByuserName(String username) {
        return userRepository.findUserByuserName(username);
    }

    @Override
    public int singUpUser(UserSingUpRequest userSingUpRequest) {
        userSingUpRequest.setCreate_date(LocalDateTime.now());
        return userRepository.singUpUser(userSingUpRequest);
    }

    @Override
    public Role findUserRoleByName(String name) {
        return rolseRepostitory.findRoleByName(name);
    }

    @Override
    public int createUserRole(int role_id, int user_id) {
        return rolseRepostitory.createUserRole(role_id,user_id);
    }

    @Override
    public int cratePost(Post post) {
        return postRepository.cratePost(post);
    }

    @Override
    public PostResponse findPostByID(int id) {
        return postRepository.findPostByID(id);
    }

    @Override
    public Post getPostById(int id) {
        return postRepository.getPostById(id);
    }

    @Override
    public int deletePostById(int id) {
        return postRepository.deletePostById(id);
    }

    @Override
    public int updatePost(int id) {
        return postRepository.updatePost(id);
    }


}
